function problem5(inventory) {

   if (!inventory || inventory.length === 0) {
    return "Sorry, the inventory is empty.";
  }
  const carYears = [];
  for (let index = 0; index < inventory.length; index++) {
    carYears.push(inventory[index].car_year);
  }
  
  const oldCars = [];
  let count = 0;
  for (let index = 0; index < carYears.length; index++) {
    if (carYears[index] < 2000) {
      oldCars.push(inventory[index]);
      count++;
    }
  }
  
  console.log(`There are ${count} cars older than the year 2000.`);
  return oldCars;
}

module.exports = problem5;

