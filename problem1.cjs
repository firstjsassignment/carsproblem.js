function problem1(inventory, carId) {
  if (typeof inventory === 'undefined'||inventory.length === 0) {
    return [];
  }
   if (typeof carId !== 'number') {
    return [];
  }
    if (!Array.isArray(inventory)) {
    return [];
  }


  if (!inventory||!typeof carId==='undefined') {
    return [];
  }

  for (let index = 0; index < inventory.length; index++) {
  var id=inventory[index]['id'];
  if(id===carId){
    return inventory[index];
  }
}
}

module.exports = problem1;

