function problem6(inventory) {
  
    if (!inventory || inventory.length === 0) {
    return "Sorry, the inventory is empty.";
  }

  const BMWAndAudi = [];
  for (let index = 0; index < inventory.length; index++) {
    const carMake = inventory[i].car_make;
    if (carMake === "BMW" || carMake === "Audi") {
      BMWAndAudi.push(inventory[index]);
    }
  }
  
  console.log(JSON.stringify(BMWAndAudi));
  return BMWAndAudi;
}

module.exports = problem6;

