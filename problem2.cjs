function problem2(inventory) {

  if (!inventory || inventory.length === 0) {
    return "Sorry, the inventory is empty.";
  }

  const lastCar = inventory[inventory.length - 1];
  const carMake = lastCar.car_make;
  const carModel = lastCar.car_model;
  return `Last car is a ${carMake} ${carModel}`;
}

module.exports = problem2;

