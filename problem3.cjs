function problem3(inventory) {

   if (!inventory || inventory.length === 0) {
    return "Sorry, the inventory is empty.";
  }

  const carModels = [];
  for (let index = 0; index < inventory.length; index++) {
    carModels.push(inventory[index].car_model);
  }

  const sortedModels = carModels.sort();
  return sortedModels;
}

module.exports = problem3;

