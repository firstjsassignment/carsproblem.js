function problem4(inventory) {
  const carYears = [];
    if (!inventory || inventory.length === 0) {
    return "Sorry, the inventory is empty.";
  }
  for (let index = 0; index < inventory.length; index++) {
    carYears.push(inventory[index].car_year);
  }
  return carYears;
}

module.exports = problem4;

